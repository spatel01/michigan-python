import pygame
import math

# intialization of pygame
pygame.init()

# set screen
screen = pygame.display.set_mode((500,500))

# ser caption
pygame.display.set_caption('PowerRangers')

# Intialization for ractangle
height = 55
width = 44
x = 40
y = 40
vel = 1

# looping 
running = True
ctn = 1
jump = False
while running:
    
    # time delay
    pygame.time.delay(5)

    # fill scren
    screen.fill((0, 0, 25)) 

    # mouce, keybord, etc event handling
    for event in pygame.event.get():
        if event.type == pygame.QUIT or event.type == pygame.K_ESCAPE:
            running = False
    key = pygame.key.get_pressed()
    if key[pygame.K_LEFT] and x > vel:
        x -= vel
    if key[pygame.K_RIGHT] and x <= 460:
        x += vel
    if not(key[pygame.K_SPACE]) and not(jump):
        if key[pygame.K_UP] and y > vel:
            y -= vel
        if key[pygame.K_DOWN] and y <= 445:
            y += vel 
    else:
        temp = y
        if ctn <= 10:
            y -= ctn
            ctn += 1
        else:
            y += ctn
            ctn -= 1
            if ctn == 2:
                ctn = 10
                jump = False
  
    pygame.draw.rect(screen, (255, 0, 0),(x, y, width, height))

    pygame.display.update()

pygame.quit()