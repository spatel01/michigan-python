import turtle

#alex = turtle.Turtle()
wn = turtle.Screen()
wn.bgcolor('black')
'''
# global intialization
alex.color('red')
alex.pensize(3)

#draw on screen
alex.forward(100)
alex.left(90)
alex.forward(50)
alex.left(90)
alex.forward(100)
alex.right(90)
alex.forward(50)
alex.right(90)
alex.forward(100)
'''
'''
wn.bgcolor('black')
draw1 = turtle.Turtle()
draw1.color('red')
draw2 = turtle.Turtle()
draw2.color('blue')

length_t = lambda x,y : (x**2 + y**2)**(1/2)
draw1.forward(100)
draw1.left(90)
draw1.forward(150)
draw1.left(135)
draw1.forward(length_t(300,150))
draw1.left(135)
draw1.forward(150)

draw2.left(180)
draw2.forward(100)
draw2.right(90)
draw2.forward(150)
draw2.right(135)
draw2.forward(length_t(300,150))
draw2.right(135)
draw2.forward(150)
'''
turtle.color('green')
turtle.shape('turtle')
turtle.up()
d1 = {1:'green',2:'red',3:'yellow',4:'blue',5:'purple',6:'white',7:'orange',8:'lightgrey',9:'violet'}
x,i = 10,0
for _ in range(100):
    turtle.stamp()
    turtle.forward(x)
    x = x + 2
    turtle.left(24)
    i = i + 1
    if i>9:
        i = 1
    turtle.color(d1[i])
    turtle.speed(5)
#till not press the exit button it will not close the windows 
wn.exitonclick()







