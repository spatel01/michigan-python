'''
Demo1 :
1.To introduce tkinter module
2.To demonstarte ttk module's widget and its method
'''

import tkinter as tk
from tkinter import ttk # ttk is used to set widget

win = tk.Tk() # creation of windows GUI

# windows form attribute or methods
# win['bg'] = "red" or win.config(bg='green') # using this two things you can change the background of the form
win.geometry("400x300") # specifing width X height of the screen
win.title("Demo1") # change the title of actionbar 

# widget - label, button, radio

name_label = ttk.Label(win,text="Enter Your Name : ")
name_label.grid(row=0,column=0,sticky=tk.W)  # grid, pack is used to set widget positionally
name_var = tk.StringVar() # create variable to get value from the entry box
name_entry = ttk.Entry(win,width=25,textvariable=name_var) # entry box to get value from the user and textvariable keyword is for getting data into the variable name_var 
name_entry.grid(row=0,column=1)
print(name_var.get())

email_label = ttk.Label(win,text="Enter Your Email : ")
email_label.grid(row=1,column=0,sticky=tk.W)  # sticky keyword is used to align all rows at same position
email_var = tk.StringVar()
email_entry = ttk.Entry(win,width=25,textvariable=email_var)
email_entry.grid(row=1,column=1)

age_label = ttk.Label(win,text="Enter Your Age : ")
age_label.grid(row=2,column=0,sticky=tk.W)  # sticky keyword is used to align all rows at same position
age_var = tk.StringVar()
age_entry = ttk.Entry(win,width=25,textvariable=age_var)
age_entry.grid(row=2,column=1)

# submit button
def action():
    name = name_var.get()
    email = email_var.get()
    age = age_var.get()
    print(f'{name} is {age} years old, Email : {email}')

submit_button = ttk.Button(win,text="Submit",command=action)
submit_button.grid(row=3,column=1)

win.mainloop() # to take action against each event triggered by the user.
