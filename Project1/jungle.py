import pygame
import random


# intilize pygame module
pygame.init()

# create screen
ScreenWidth = 500
ScreenHeight = 500
screen = pygame.display.set_mode((ScreenWidth, ScreenHeight))

# pygame caption or icon
pygame.display.set_caption("JungleFighter")
icon = pygame.image.load('Game.png')
pygame.display.set_icon(icon)


# Player
playerX = 50
playerY = 250
player_change = 0

# Enemies
enemyX = random.randint(400,500)
enemyY = random.randint(30,470)
enemy_changeX = 0
enemy_changeY = 0

# bullet

# circle
circleX = 250
circleY = 250
c_x_change = 0
c_y_change = 0

# FOR CIRCLE
def circle(x, y):
    pygame.draw.circle(screen, (0, 0, 255), (x, y), 30)


running = True
while running:

    # when hit the exit button while loop will be terminate
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                c_x_change = -1
            if event.key == pygame.K_RIGHT:
                c_x_change = 1
            if event.key == pygame.K_UP:
                c_y_change = -1
            if event.key == pygame.K_DOWN:
                c_y_change = 1
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                c_x_change = 0
            if event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                c_y_change = 0
    
    # fill the background 
    screen.fill([100,100,100])

    # CIRCLE MOVEMENT 
    circleX = circleX + c_x_change
    circleY = circleY + c_y_change
    # for X cordinate
    if circleX > 470:
        circleX = 470
    elif circleX < 30:
        circleX = 30
    # for Y cordinate
    if circleY > 470:
        circleY = 470
    elif circleY < 30:
        circleY = 30


    circle(circleX,circleY)

    # update display frame each and every on iteration
    pygame.display.update()

# after the and of the loop
pygame.quit()
